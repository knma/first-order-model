import os, sys
from argparse import ArgumentParser

import cv2
import time
import numpy as np

import matplotlib
matplotlib.use('Agg')
import yaml

import imageio
from skimage.transform import resize
from skimage import img_as_ubyte
import torch
from sync_batchnorm import DataParallelWithCallback

from modules.generator import OcclusionAwareGenerator
from modules.keypoint_detector import KPDetector
from animate import normalize_kp
from scipy.spatial import ConvexHull

import ctypes 
import queue, threading

class VideoCapture:
    def __init__(self, name):
        self.cap = cv2.VideoCapture(name)
        if not self.cap.isOpened():
            sys.exit('wrong driving stream')
        self.q = queue.Queue()
        self.t = threading.Thread(target=self._reader)
        self.t.daemon = True
        self.t.start()
    def _reader(self):
        while True:
            ret, frame = self.cap.read()
            if not ret:
                break
            if not self.q.empty():
                try:
                    self.q.get_nowait() 
                except Queue.Empty:
                    pass
            self.q.put(frame)
    def read(self):
        return self.q.get()
    def get_tid(self):
        if hasattr(self.t, '_thread_id'): 
            return self.t._thread_id 
        for id, thread in threading._active.items(): 
            if thread is self.t: 
                return id
    def terminate(self):
        t_id = self.get_tid()
        if (ctypes.pythonapi.PyThreadState_SetAsyncExc(t_id, ctypes.py_object(SystemExit)) > 1):
            ctypes.pythonapi.PyThreadState_SetAsyncExc(t_id, 0) 
        self.t.join() 


def load_checkpoints(config_path, checkpoint_path, cpu=False):

    with open(config_path) as f:
        config = yaml.load(f)

    generator = OcclusionAwareGenerator(**config['model_params']['generator_params'],
                                        **config['model_params']['common_params'])
    if not cpu:
        generator.cuda()

    kp_detector = KPDetector(**config['model_params']['kp_detector_params'],
                             **config['model_params']['common_params'])
    if not cpu:
        kp_detector.cuda()
    
    if cpu:
        checkpoint = torch.load(checkpoint_path, map_location=torch.device('cpu'))
    else:
        checkpoint = torch.load(checkpoint_path)
 
    generator.load_state_dict(checkpoint['generator'])
    kp_detector.load_state_dict(checkpoint['kp_detector'])
    
    if not cpu:
        generator = DataParallelWithCallback(generator)
        kp_detector = DataParallelWithCallback(kp_detector)

    generator.eval()
    kp_detector.eval()
    
    return generator, kp_detector

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--cpu", dest="cpu", action="store_true", help="cpu mode")

    parser.add_argument("--config", default='config/vox-256.yaml', help="path to config")
    parser.add_argument("--checkpoint", default='vox-cpk.pth.tar', help="path to checkpoint to restore")

    parser.add_argument("--relative", dest="relative", action="store_true", help="use relative or absolute keypoint coordinates")
    parser.add_argument("--adapt_scale", dest="adapt_scale", action="store_true", help="adapt movement scale based on convex hull of keypoints")

    parser.add_argument("--source_image_dir", default='data/source_images', help="source images dir")
    parser.add_argument("--stream", default='0', help="path to driving video stream")
    parser.add_argument("--result_video", default='result.mp4', help="path to output")
    parser.add_argument("--result_video_fps", type=int, default=0)

    parser.add_argument("--save_video", action="store_true")
    parser.add_argument("--save_video_max_frames", type=int, default=1000, help="max frames to save")
 
    parser.set_defaults(relative=True)
    parser.set_defaults(adapt_scale=True)

    opt = parser.parse_args()

    generator, kp_detector = load_checkpoints(config_path=opt.config, checkpoint_path=opt.checkpoint, cpu=opt.cpu)

    sources = {}
    source_images = sorted(os.scandir(opt.source_image_dir), key=lambda e: e.name)
    for source_image in source_images:
        try:
            i = int(os.path.splitext(source_image.name)[0])
            sources[i] = imageio.imread(source_image.path)
            print(f'{source_image.name}: ok')
        except ValueError:
            continue
    if len(sources) == 0:
        sys.exit('Place source images to data/source_images dir')

    cam_i = 0
    if len(opt.stream) < 3:
        try:
            cam_i = int(opt.stream)
        except ValueError:
            pass

    vc = VideoCapture(opt.stream if len(opt.stream) > 2 else cam_i + cv2.CAP_ANY)

    previews = []
    size = 256
    current_source_i = 1
    current_relative = opt.relative
    kp_driving_initial = None
    need_up_kp_initial = False
    t_prev = 0
    dt = 1

    try:
        with torch.no_grad():
            for source_i, source_image in sources.items():
                source_image = resize(source_image, (256, 256))[..., :3]
                source_tensor = torch.tensor(source_image[np.newaxis].astype(np.float32)).permute(0, 3, 1, 2)
                if not opt.cpu:
                    source_tensor = source_tensor.cuda()
                sources[source_i] = (source_image, source_tensor, kp_detector(source_tensor))

            while True:
                try:
                    frame = vc.read()
                    frame_size = frame.shape[:2]
                    frame_side = np.min(frame_size)
                except:
                    print('Couldn\'t fetch a frame')
                    time.sleep(0.5)
                    continue

                y_pad = (frame_size[0]-frame_side)//2
                x_pad = (frame_size[1]-frame_side)//2
                frame = frame[y_pad:y_pad+frame_side,x_pad:x_pad+frame_side]
                frame = cv2.resize(frame, (size, size))
                frame = np.flipud(frame)
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB).astype(np.float32) / 255

                if 1:
                    driving_frame = torch.tensor(frame.copy()[np.newaxis]).permute(0, 3, 1, 2)
                    if not opt.cpu:
                        driving_frame = driving_frame.cuda()

                    kp_driving = kp_detector(driving_frame)

                    if kp_driving_initial is None or need_up_kp_initial:
                        kp_driving_initial = kp_driving
                        need_up_kp_initial = False

                    kp_norm = normalize_kp(kp_source=sources[current_source_i][2], kp_driving=kp_driving,
                                           kp_driving_initial=kp_driving_initial, use_relative_movement=current_relative,
                                           use_relative_jacobian=current_relative, adapt_movement_scale=opt.adapt_scale)
                    out = generator(sources[current_source_i][1], kp_source=sources[current_source_i][2], kp_driving=kp_norm)

                    out_frame = np.transpose(out['prediction'].data.cpu().numpy(), [0, 2, 3, 1])[0]

                    source_image = sources[current_source_i][0]
                    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                    frame = np.stack([frame,frame,frame], axis=2)
                    preview = np.hstack([source_image, frame, out_frame])
                    preview = (preview * 255).astype(np.uint8)
                    preview = np.clip(preview, 0, 255)
                    preview = cv2.cvtColor(preview, cv2.COLOR_RGB2BGR)

                    fps = 1/dt
                    cv2.putText(
                        preview, f'fps: {fps:.2f}', 
                        (10,20), 
                        cv2.FONT_HERSHEY_SIMPLEX, 
                        0.4,
                        (255,255,255),
                        1
                    )

                    result = (out_frame * 255).astype(np.uint8)
                    result = np.clip(result, 0, 255)
                    result = cv2.cvtColor(result, cv2.COLOR_RGB2BGR)

                    if opt.save_video:
                        previews.append(cv2.cvtColor(preview, cv2.COLOR_BGR2RGB))

                else:
                    preview = frame

                t_now = time.time()
                dt = t_now-t_prev
                t_prev = t_now

                cv2.imshow("Meeting preview", preview)
                cv2.imshow("Meeting stream", result)

                key = cv2.waitKey(1)
                if key == 27: # ESC
                    break
                if key == 32: # space
                    print(f'kp reset')
                    need_up_kp_initial = True
                if key == 114: # r
                    current_relative = not current_relative
                    print(f'relative kp: {current_relative}')
                if key in range(49, 58) and key-48 in sources:
                    current_source_i = key-48
                    print(f'current source: {current_source_i}')

        if opt.save_video:
            imageio.mimsave(opt.result_video, [img_as_ubyte(frame) for frame in previews], fps=opt.result_video_fps if opt.result_video_fps > 0 else fps)

        print('shutting down..')
        vc.terminate()
    except KeyboardInterrupt:
        vc.terminate()

    cv2.destroyWindow("Meeting preview")
    cv2.destroyWindow("Meeting stream")
