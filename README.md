# Online meetings with arbitrary faces

### Requirements
CUDA-enabled GPU

## Windows

1. ### Setup environment
* Get [**Miniconda**](https://repo.anaconda.com/miniconda/Miniconda3-latest-Windows-x86_64.exe) and install
* Open **Anaconda Prompt** and create **python 3.7** environment: **``` conda create -n cam_speech python=3.7 ```**
* Activate the environment **``` conda activate cam_speech ```**
* Install **PyTorch**:  **```conda install pytorch==1.2.0 torchvision==0.4.0 cudatoolkit=10.0 -c pytorch```**
* Install **pip packages**:  **```pip install -r requirements.txt```**

2. ### Get [**ManyCam**](https://manycam.com/) or any other webcam altering software

3. ### Prepare source images
* Place cropped images to **data/source_images** directory

4. ### Run **``` python cam_demo.py [arguments] ```** 
- opt **stream** — driving video stream (e.g., *http://192.168.0.100:8080/video*); 0 - 9 for ordinary web camera
- opt **source_image_dir** — source images directory
- opt **relative** — relative keypoints (requires calibration)
- opt **save_video** — save preview video to file
- opt **save_video_max_frames** — max frames to save
- opt **result_video** — result video path
- opt **result_video_fps**

#### Hotkeys
- **1 - 9** — current source image (corresponds to images in source_image_dir, e.g., **01.png** -> **1**, **05.png** -> **5**)
- **r** — toggle relative/absolute keypoints
- **space** — calibrate keypoints
- **esc** — exit
